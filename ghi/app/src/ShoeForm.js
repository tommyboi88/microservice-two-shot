import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [shoeName, setShoeName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [shoeColor, setShoeColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const handleShoeNameChange = (event) => {
        const value = event.target.value;
        setShoeName(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleShoeColorChange = (event) => {
        const value = event.target.value;
        setShoeColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            shoeName: shoeName,
            manufacturer: manufacturer,
            shoeColor: shoeColor,
            pictureUrl: pictureUrl,
            binLocation: binLocation,

        };
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig =  {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if(response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setShoeName(' ');
            setManufacturer(' ');
            setShoeColor(' ');
            setPictureUrl(' ');
            setBinLocation(' ');
        }
    }

    useEffect(() => {
        const grabBins = async () => {
            const url = 'http://localhost:8100/api/bins/';

            const response = await fetch(url);
            if(response.ok) {

                const data = await response.json();
                setBins(data.bins)
            }
        }
        grabBins();
    }, []);


    return (
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input onChange={handleShoeNameChange} value={shoeName} placeholder="Shoe name" required type="text" id="shoe_name" className="form-control" />
              <label htmlFor="shoe_name">Shoe name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleShoeColorChange} value={shoeColor} placeholder="Shoe color" type="text" id="shoe_color" className="form-control" />
              <label htmlFor="shoe_color">Shoe color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="picture url" required type="url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinLocationChange} value={binLocation} required className="form-select" name="bin" id="bin">
                <option value="bin">Select a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.id}>{bin.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    );
}

export default ShoeForm;
