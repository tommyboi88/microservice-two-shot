from django.contrib import admin
from .models import Shoe, BinVO


@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = ["bin_name", "bin_number", "bin_href", "id"]



@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = ["shoe_name", "manufacturer", "shoe_color", "picture_url", "id"]
