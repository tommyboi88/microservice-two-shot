from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    bin_name = models.CharField(max_length=100)
    bin_number = models.IntegerField(default=0)
    bin_href = models.CharField(max_length=100, default="default_value", unique=True)


# Create your models here.
class Shoe(models.Model):
    shoe_name= models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    shoe_color = models.CharField(max_length=100)
    picture_url = models.URLField(default=" ")

    shoe_bin = models.ForeignKey(BinVO, on_delete=models.CASCADE, related_name="shoes", null=True)

    def __str__(self):
        return self.shoe_name

    def get_api_url(self):
        return reverse("api_shoe_detail", kwargs={"id": self.id})
