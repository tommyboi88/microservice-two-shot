from django.shortcuts import render
from django.http import  JsonResponse
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "bin_name",
        "bin_number",
        "bin_href",
        "id",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties =[
        "shoe_name",
        "manufacturer",
        "shoe_color",
        "picture_url",
        "id",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties =[
        "shoe_name",
        "manufacturer",
        "shoe_color",
        "picture_url",
        "id",


    ]



@require_http_methods(["GET", "POST"])
def api_shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
        {"shoes": shoes},
        encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "DELETE"])
def api_shoe_detail(request, id):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        if request.method == "DELETE":
            count, _ = Shoe.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0  }
            )
