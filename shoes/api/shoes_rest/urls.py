from django.urls import path
from .views import  api_shoe_detail , api_shoe_list

urlpatterns = [
    path("shoes/", api_shoe_list, name="api_shoe_list"),
    path("shoes/<int:id>/", api_shoe_detail, name="api_shoe_detail"),
]
